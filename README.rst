Chess
==========================================================

Project Description:
--------------------

Old, old, old, chess program I wrote while taking an AI class.  The original goal of the project was to write a minimax algorithm to play tic-tac-toe, and I took it further by implementing the rules of chess, adding alpha-beta pruning, and null move heuristic.  It actually worked!  So I keep it here so I don't lose it.
